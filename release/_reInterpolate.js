/// <reference types='./_reInterpolate.d.ts' />

/** Used to match template delimiters. */
var reInterpolate = /<%=([\s\S]+?)%>/g;

export default reInterpolate;
