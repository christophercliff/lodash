/// <reference types='./_Symbol.d.ts' />

import root from './_root.js';

/** Built-in value references. */
var Symbol = root.Symbol;

export default Symbol;
