/// <reference types='./_getPrototype.d.ts' />

import overArg from './_overArg.js';

/** Built-in value references. */
var getPrototype = overArg(Object.getPrototypeOf, Object);

export default getPrototype;
