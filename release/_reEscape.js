/// <reference types='./_reEscape.d.ts' />

/** Used to match template delimiters. */
var reEscape = /<%-([\s\S]+?)%>/g;

export default reEscape;
