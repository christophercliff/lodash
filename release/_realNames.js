/// <reference types='./_realNames.d.ts' />

/** Used to lookup unminified function names. */
var realNames = {};

export default realNames;
