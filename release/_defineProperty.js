/// <reference types='./_defineProperty.d.ts' />

import getNative from './_getNative.js';

var defineProperty = (function() {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

export default defineProperty;
