/// <reference types='./_reEvaluate.d.ts' />

/** Used to match template delimiters. */
var reEvaluate = /<%([\s\S]+?)%>/g;

export default reEvaluate;
